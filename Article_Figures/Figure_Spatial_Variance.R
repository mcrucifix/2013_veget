print ( load('../Data/LOVECLIM.rda') )
print ( load('../Data/LOVECLIM_PCA_GP.rda') )

require(fields)

pdf('Figure_variance.pdf',9,3.6  , pointsize=10)
par(mfrow=c(1,3))
par(oma=c(0,2,2,2))
par(mar=c(8,3,3,3))
par(las=1)



dabs <- function(x) {(abs(x))}
mdabs <- function(x) {mean(dabs(x))}
varnames = c('pann','gdd','tann')


vartitle = c(pann='Annual precipitation (log)', 
             gdd=expression(paste("GDD (",degree,"C)"^2)), 
             tann=expression(paste("Annual temperature (",degree,"C)"^2)))

for (varname in varnames)
{
   
  # considers the logarithm for pann only
  mlog = function(x) x 
  if ( varname=='pann' ) mlog = log



  # discarded PC
  OUT = sweep( PCA[[varname]]$PCA, 3, PCA[[varname]]$d, '*')
  discarded_pc <- apply ( OUT[,,11:26]^2, c(1,2), sum) / nrow(PCA[[varname]]$amps)

  # analytical vs monte-carlo
  print ( load('../Data/LOVECLIM_PCA_GSA_unif.rda') )
  total       <-  M_const[[varname]]$Mtot
  gpvar       <-  M_const[[varname]]$Stot
  synergy     <-  M_const[[varname]]$Ms
  anal_vs_mc  <-  M_const[[varname]]$Mtot - M_const_anal[[varname]]$Mtot   +
                  M_const[[varname]]$Stot - M_const_anal[[varname]]$Stot 

  # pca emulator vs independent emulator

  # diff between experiments
  d = (mlog(var$von[[varname]][,,-20]) - mlog(var$von35[[varname]][,,-20]))^2
  diffexp <- apply(d, c(1,2), mean)


  # emulator variance
  ev <- M_const_anal[[varname]]$Stot

  # independent vs pca emulator
  local({
    load('../Data/GSA_Ind_Emul.rda')
    M_indemul <<- M_const
  })


  nd_vs_pca <- M_indemul[[varname]]$Mtot - M_const_anal[[varname]]$Mtot 

  # const vs non-constant
  const_no_const <- M[[varname]]$Mtot - M_const[[varname]]$Mtot

  source('showmap.R')

  s <- function(v, s) 
  { showmap ( abs(v), main=sprintf( '%s - mean abs. diff = %.4f ', s, mean(abs(v))), amp=.3) }

# par(mfrow=c(3,2))
# s(discarded_pc, 'Discarded PCs')
# s(anal_vs_mc, 'MC samping')
# s(diffexp, 'Initial conditions')
# s(ev, 'Emulator variance')
# s(nd_vs_pca,'pca_emul vs ind. emul')
# s(const_no_const,'const vs no-const hyperparams')

  bars <- c( 
          "Total variance"=mdabs(total),
          "Discarded PC "= mdabs(discarded_pc),
          "MC sampling"=mdabs(anal_vs_mc),
          "GP var "=mean(gpvar),
          "GP params"=mdabs(const_no_const),
          "PC emus vs indep. emul" = mdabs(nd_vs_pca),
          "initial cond."=mdabs(diffexp),
          "Synergy"=mdabs(synergy)
          )

print(bars)

percentages = as.numeric(bars / bars[1])[-1]

labels = names(bars)
#labels[7] = expression(V^(pc))
#labels[8] = expression(V^(gp))
barplot(bars, xlab="", xaxt = "n", log='');
y = (par("usr")[3]) - 0.04*((par("usr")[4])-(par("usr")[3]))
text(((1:8)-1)*1.12+1, y , srt = 45, adj = 1,
          labels = labels,  xpd = TRUE)

text((2:8)*1.19-0.3,  -2*y , srt = 0, sprintf('%1.1f%%', percentages*100) )



mtext(vartitle[varname],3,2)

}
dev.off()
