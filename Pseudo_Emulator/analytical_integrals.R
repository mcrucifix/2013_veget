require(gsl)



mc_intxe <- function(x1, l=1)
{
x <- function(x)  x*exp(-(x-x1)^2/ l^2);
y = runif(10000)
mean(x(y))
}

l = 1


analy_intxe <- function(x1, l=1)
{
dummy <- function(x1)
{
if (x1<1)
{
 if (x1 < 0)
 {
 - (gamma_inc(0.5, (x1^2 - 2*x1 + 1)/l^2) * l * x1 ) /  2   +
 (gamma_inc(0.5, x1^2 /l^2) * l * x1 ) / 2  -
 (gamma_inc(1, (x1^2 - 2*x1 + 1)/l^2) * l^2 ) / 2 + 
 (gamma_inc(1, x1^2 / l^2) * l^2) / 2 
 } else 
 {
 (gamma_inc(0.5, (x1^2 - 2*x1 + 1)/l^2) * l * (x1^2-x1) ) / (2 * (1-x1)) - 
 (gamma_inc(0.5, x1^2 /l^2) * l * x1 ) / 2  -
 (gamma_inc(1, (x1^2 - 2*x1 + 1)/l^2) * l^2 ) / 2 + 
 (gamma_inc(1, x1^2 / l^2) * l^2) / 2 +
 sqrt(pi)*l*x1 
 }
} else 
{
 (gamma_inc(0.5, (x1^2 - 2*x1 + 1)/l^2) * l * x1 ) /  2     - 
 (gamma_inc(0.5, x1^2 /l^2) * l * x1 ) / 2  -
 (gamma_inc(1, (x1^2 - 2*x1 + 1)/l^2) * l^2 ) / 2 + 
 (gamma_inc(1, x1^2 / l^2) * l^2) / 2 
}
}
Vectorize(dummy)(x1)
}


mc_inte <- function(x1, l=1)
{
x <- function(x)  exp(-(x-x1)^2/ l^2);
y = runif(10000)
mean(x(y))
}


analy_inte <- function(x1, l=1)
{
 sqrt(pi)/2 * l * ( erf(x1/l) - erf((x1-1)/l))
}

mc_inttt <- function(x1, x2=0., l=1)
{
 x <- function(x)  exp(-(x-x1)^2/ l^2) *  exp(-(x-x2)^2/ l^2) 
 y = runif(100000)
mean(x(y))
}


analy_inttt <- function(x1, x2=0., l=1)
{
 s2 = sqrt(2)
 sqrt(pi)*l/(2^(1.5)) * exp(-(x1-x2)^2/(2*l^2)) * ( erf((x1+x2)/(s2 * l ))  - erf((x1+x2-2)/(s2 * l ))  )
}

mc_dblint <- function(l)
{
 x=runif(1000)
 y=runif(1000)
 F <- function(x,y)  exp(-(x-y)^2/ l^2) 
 mean(outer(x,y,F))
}


analy_dblint <- function(l)
{
 ( - l^2 + sqrt(pi)*erf(1/l)*l + l*l * exp(-1/l^2) ) 
}



Up <- function(X, lambda, p)
{
 n = nrow(X)
 k = ncol(X)
 
 if ( !is.null (p) ) mp = seq(k)[-p] else mp = seq(k)
 
 theta = lambda$theta
 Up = 1;
 for (i in mp)
  Up = Up * analy_dblint(theta[i])
 if ( length(mp) == 0) Up = Up + lambda$nugget
 return(Up)
}

Pp <- function (X, lambda, p)
{
 theta = lambda$theta
 n = nrow(X)
 k = ncol(X)
 Pp = matrix(1, n, n )
 if ( is.null (p) ) p=9999
 for (i in seq(k))
 {
   if (any ( i == p) ) 
    Pp = Pp * outer(X[,i], X[,i], function(x,y) analy_inttt(x, y, l=theta[i]))
   else 
    Pp = Pp * outer(X[,i], X[,i], function(x,y) analy_inte(x, l=theta[i]) * analy_inte( y, l=theta[i] ) )
 }
 Pp
}

Sp <- function (X, lambda, p)
{
 theta = lambda$theta
 n = nrow(X)
 k = ncol(X)
 if ( is.null (p) ) p=9999

 line1 = rep(1, n)
 for (i in seq(k)) line1 = line1 * analy_inte(X[,i], l=theta[i]) 
 Sp = line1 


 for (j in seq(k))
 {
   line1 = rep(1, n)
   for (i in seq(k))
   {
     if ( i == j )
     {
     if (any ( i == p) ) 
      line1 = line1 * analy_intxe(X[,i] , l=theta[i]) 
     else 
      line1 = line1* 1/2 * analy_inte(X[,i], l=theta[i]) 
     } 
     else 
     line1 = line1* analy_inte(X[,i], l=theta[i]) 

   }
   Sp = rbind(Sp, line1)
 }
 Sp
}


Qp <- function (X, p)
{
 k = ncol(X)
 line1 = c(1, rep(1/2, k))
 Qp = line1
 for (i in seq(k))
 {
   line1 = rep(1/4, k)
   if (any ( i == p) ) line1[i] = 1/3
   line1 = c(1/2, line1)
   Qp = rbind(Qp, line1)
 }

 Qp
}


