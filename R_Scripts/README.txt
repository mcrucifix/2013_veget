# execute in sequence:
# ---------------------

# generate_XX
# -----------
#
# generate 2 times 10000 points (astronomical distrubiotns)
# to be used for the sensitivity analysis
# output : XX (stored in ../Data/XX.rda


# optim_lambda_nu_pca.R :
# -----------------------
#  input : LOVECLIM.rda (LOVECLIM inputs)
#  uses  : rhoarray.R (reads astronomical forcing and computes density)
#          GP package : Crucifix' package for Gaussian Process emulation
#          pca_emul :  add-on specifically for PCA analysis
#  purpose: performs PCA, does the barplot
#  output : in ../Data/LOVECLIM_PCA_GP.rda : 
#           GPs:       list of Gaussian process  associated to PCA scores
#           GPs_const: same but with constant hyperparams
#           hp       : array of hyperparams
#           hp_const : same with constant hyperparams
#           bp       : barplot data
#           bp_const : barplot data with constant hyperparams
#           ll       : likelihoods
#           ll_const : likelihoods with constant hyperparams

# ADDENDUM:
# independent_emul.R : 
# is a copy-paste of optim_lambda_nu_pca.R, and hacked for independent emulators
# as requested after review #1 of the paper

# 


# compute_sensitivity_indices.R
# -----------------------------
#
#  input : ../Data/LOVECLIM_PCA_GP.rda (supplied by the above)
#          ../Data/XX.rda
#  uses  :  gsa_2.R (suppling gsa_constant and gsa_int_constant)
#           note : gsa_2.R depends itself on 'analytical integrals'
#  purpose: computes global sensitivity indices
#  output : GS, a list containing: 
#             a series of combination of indices (p13, p23, p12, obl, etc. )
#             each of which being a list containing GSA and GSA_const 
#             stored in LOVECLIM_PCA_GS_with_p13_p23.rda
#           a similar list but with uniform distributions (and not astronomical ones)
#           is stored in LOVECLIM_PCA_GS_unif.rda

# provide_analytical_integrals.R
# -----------------------------
# same as compute_sensitivity_indices.R but
# output: GSAnal : a list similar as GS but with analytical integrals
#

# compute_sensitivity_maps.R
# --------------------------
# inputs:
#    LOVECLIM_PCA_GP.rda (supplied by the above) 
#     (or LOVECLIM_PCA_GS.rda, where the p13 and p23 indices
#                                    where not supplied)
#    LOVECLIM_PCA_GSA_unif.rda
#  uses: 
#    pca_emul (for pca reconstruction)
#  purpose : computes geographical maps of sensitivity indices, based on the sensitivity indices
#         supplied per PC
#  output : 
#           M_const and Mbar_const : mean effect variances and total effect variances
#           to be used by graphic plotting routine

# compute_sensitivity_covariances.R
# ---------------------------------
# same as compute sensitivity maps but with 'reconstruct_var' redifined
# in order to provide full covariance matrices (and not grid-point wise only)
# to be supplied to  "fingerprints.R". Added after the first revision
#
# for astronomical distribution only
#
# provides: 
#  LOVECLIM_PCA_GSA_COVAR.rda  
#
# fingerprints.R
# --------------
#  input : M_const supplied in  LOVECLIM_PCA_GSA_COVAR.rda
#  performs svd decomposition of M_const
#  output : 
#    FINGERPRINTS, in "Data/Fingerprints.rda"

# African_precession.R
# --------------------
# inputs:
#  LOVECLIM_PCA_GP.rda
# uses: 
#  pca_emul.R
# purpose:
#  simulate precipitation along an idealised precession cycle
# output: 
#  this is a self 'compute-and-show' routine that directly supplies the 'pdf'
#  'African_precip.pdf'
#
