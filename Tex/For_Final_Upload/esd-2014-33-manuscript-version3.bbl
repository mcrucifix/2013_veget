\begin{thebibliography}{81}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{{\tt #1}}
\providecommand{\urlprefix}{URL }
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi:\discretionary{}{}{}#1}\else
  \providecommand{\doi}{doi:\discretionary{}{}{}\begingroup
  \urlstyle{rm}\Url}\fi

\bibitem[{Alpert and Sholokhman(2011)}]{Alpert11aa}
Alpert, P. and Sholokhman, T., eds.: Factor Separation in the atmosphere:
  applications and future prospects, Cambrdige University Press, Cambridge, UK,
  2011.

\bibitem[{Andrianakis and Challenor(2012)}]{Andrianakis12aa}
Andrianakis, I. and Challenor, P.~G.: The effect of the nugget on Gaussian
  process emulators of computer models, Computational Statistics \& Data
  Analysis, 56, 4215--4228, \doi{10.1016/j.csda.2012.04.020}, 2012.

\bibitem[{Araya-Melo et~al.(2015)Araya-Melo, Crucifix, and
  Bounceur}]{Araya-Melo15aa}
Araya-Melo, P.~A., Crucifix, M., and Bounceur, N.: Global sensitivity analysis
  of the Indian monsoon during the Pleistocene, Climate of the Past, 11,
  45--61, \doi{10.5194/cp-11-45-2015}, 2015. 

\bibitem[{Berger(1978{\natexlab{a}})}]{Berger1978139}
Berger, A.: Long-term variations of caloric insolation resulting from the
  earth's orbital elements, Quaternary Research, 9, 139 -- 167,
  \doi{10.1016/0033-5894(78)90064-9}, 1978{\natexlab{a}}.

\bibitem[{Berger(1999)}]{berger99}
Berger, A.: The role of {CO$_2$}, sea-level and vegetation during the
  {M}ilankovitch-forced glacial-interglacial cycles, in: Proceedings
  "Geosphere-Biosphere Interactions and Climate", Pontifical Academy of
  Sciences, Vatican City, 9-13 November 1998., pp. 119--146, 1999.

\bibitem[{Berger(1978{\natexlab{b}})}]{berger78}
Berger, A.~L.: Long-term variations of daily insolation and {Q}uaternary
  climatic changes, Journal of the Atmospheric Sciences, 35, 2362--2367,
  \doi{10.1175/1520-0469(1978)035<2362:LTVODI>2.0.CO;2}, 1978{\natexlab{b}}.

\bibitem[{Berger et~al.(2001)Berger, De~Oliveira, and Sans{\'o}}]{Berger01aa}
Berger, J.~O., De~Oliveira, V., and Sans{\'o}, B.: Objective Bayesian Analysis
  of Spatially Correlated Data, Journal of the American Statistical
  Association, 96, 1361--1374, \doi{10.1198/016214501753382282}, 2001.

\bibitem[{Bosmans et~al.(2015)Bosmans, Hilgen, Tuenter, and
  Lourens}]{Bosmans15aa}
Bosmans, J. H.~C., Hilgen, F.~J., Tuenter, E., and Lourens, L.~J.: Obliquity
  forcing of low-latitude climate, Climate of the Past Discussions, 11,
  221--241, \doi{10.5194/cpd-11-221-2015}, 2015. 

\bibitem[{Braconnot et~al.(1999)Braconnot, Joussaume, Marti, and
  de~Noblet}]{Braconnot99aa}
Braconnot, P., Joussaume, S., Marti, O., and de~Noblet, N.: Synergistic
  feedbacks from ocean and vegetation on the African Monsoon response to
  Mid-Holocene insolation, Geophysical Research Letters, 26, 2481--2484,
  \doi{10.1029/1999GL006047}, 1999. 

\bibitem[{Braconnot et~al.(2007)Braconnot, Otto-Bliesner, Harrison, Joussaume,
  Peterschmitt, Abe-Ouchi, Crucifix, Driesschaert, Fichefet, Hewitt, Kageyama,
  Kitoh, La{\^\i}n{\'e}, Loutre, Marti, Merkel, Ramstein, Valdes, Weber, Yu,
  and Zhao}]{braconnot07pmip1}
Braconnot, P., Otto-Bliesner, B.~L., Harrison, S., Joussaume, S., Peterschmitt,
  J.-Y., Abe-Ouchi, A., Crucifix, M., Driesschaert, E., Fichefet, T., Hewitt,
  C.~D., Kageyama, M., Kitoh, A., La{\^\i}n{\'e}, A., Loutre, M.-F., Marti, O.,
  Merkel, U., Ramstein, G., Valdes, P., Weber, S.~L., Yu, Y., and Zhao, Y.:
  Results of {P}{M}{I}{P}2 coupled simulations of the {M}id {H}olocene and
  {L}ast {G}lacial {M}aximum -- {P}art 1: experiments and large-scale features,
  Climate of the Past, 3, 261--277, \doi{10.5194/cp-3-261-2007}, 2007. 

\bibitem[{Brovkin et~al.(1997)Brovkin, Ganopolski, and Shvirezhev}]{brovkin97}
Brovkin, V., Ganopolski, A., and Shvirezhev, Y.: A continuous
  climate-vegetation classification for use in climate-biosphere studies,
  Ecological Modelling, 101, 251--261, \doi{10.1016/S0304-3800(97)00049-5},
  1997.

\bibitem[{Brovkin et~al.(1998)Brovkin, Claussen, Petoukhov, and
  Ganopolski}]{Brovkin98aa}
Brovkin, V., Claussen, M., Petoukhov, V., and Ganopolski, A.: On the stability
  of the atmosphere-vegetation system in the Sahara/Sahel region, J. Geophys.
  Res., 103, 31\,613--31\,624, \doi{10.1029/1998JD200006}, 1998. 

\bibitem[{Brovkin et~al.(2003)Brovkin, Levis, Loutre, Crucifix, Claussen,
  Ganopolski, Kubatzki, and Petoukhov}]{brovkin03cc}
Brovkin, V., Levis, S., Loutre, M.~F., Crucifix, M., Claussen, M., Ganopolski,
  A., Kubatzki, C., and Petoukhov, V.: Stability analysis of the
  climate-vegetation system in the northern high latitudes, Climatic Change,
  57, 119--138, \doi{10.1023/A:1022168609525}, 2003.

\bibitem[{Carnell(2012)}]{Carnell12aa}
Carnell, R.: lhs: Latin Hypercube Samples,
  \urlprefix\url{http://CRAN.R-project.org/package=lhs}, r package version
  0.10, 2012.

\bibitem[{Carslaw et~al.(2013)Carslaw, Lee, Reddington, Pringle, Rap, Forster,
  Mann, Spracklen, Woodhouse, Regayre, and Pierce}]{Carslaw13aa}
Carslaw, K.~S., Lee, L.~A., Reddington, C.~L., Pringle, K.~J., Rap, A.,
  Forster, P.~M., Mann, G.~W., Spracklen, D.~V., Woodhouse, M.~T., Regayre,
  L.~A., and Pierce, J.~R.: Large contribution of natural aerosols to
  uncertainty in indirect forcing, Nature, 503, 67--71,
  \doi{10.1038/nature12674},  2013.

\bibitem[{Claussen(2009)}]{Claussen09aa}
Claussen, M.: Late Quaternary vegetation-climate feedbacks, Climate of the
  Past, 5, 203--216, \doi{10.5194/cp-5-203-2009}, 2009. 

\bibitem[{Claussen et~al.(1999)Claussen, Kubatzki, Brovkin, Ganopolski,
  Hoelzmann, and Pachur}]{claussen99}
Claussen, M., Kubatzki, C., Brovkin, V., Ganopolski, A., Hoelzmann, P., and
  Pachur, H.-J.: Simulation of an abrupt change in {Saharan} vegetation in the
  mid-{H}olocene, Geophysical Research Letters, 26, 2037--2040,
  \doi{10.1029/1999GL900494,}, 1999.

\bibitem[{Cressie(1993)}]{Cressie93aa}
Cressie, N.: Statistics for Spatial Data, Wiley series in probability and
  statistics, John Wiley \& Sons, Inc., Chichester, UK., 1993.

\bibitem[{Crucifix(2011)}]{Crucifix11aa}
Crucifix, M.: How can a glacial inception be predicted?, The Holocene, 21,
  831--842, \doi{10.1177/0959683610394883}, 2011.

\bibitem[{Crucifix and Loutre(2002)}]{crucifix02aa}
Crucifix, M. and Loutre, F.: Transient simulations over the last interglacial
  period (126-115 kyr BP): feedback and forcing analysis, Climate Dynamics, 19,
  417--433, \doi{10.1007/s00382-002-0234-z}, 2002.

\bibitem[{Cumming and Goldstein(2009)}]{Cumming09aa}
Cumming, J.~A. and Goldstein, M.: Small Sample Bayesian Designs for Complex
  High-Dimensional Models Based on Information Gained Using Fast
  Approximations, Technometrics, 51, 377--388, \doi{10.1198/TECH.2009.08015}, 2009. 

\bibitem[{de~Noblet et~al.(1996)de~Noblet, Prentice, Joussaume, Texier, Botta,
  and Haxeltine}]{noblet96}
de~Noblet, N., Prentice, I.~C., Joussaume, S., Texier, D., Botta, A., and
  Haxeltine, A.: Possible role of atmosphere-biosphere interactions in
  triggering the last glaciation., Geophysical Research Letters, 23,
  3191--3194, \doi{10.1029/96GL03004}, 1996.

\bibitem[{Dekker et~al.(2010)Dekker, de~Boer, Brovkin, Fraedrich, Wassen, and
  Rietkerk}]{Dekker10aa}
Dekker, S.~C., de~Boer, H.~J., Brovkin, V., Fraedrich, K., Wassen, M.~J., and
  Rietkerk, M.: Biogeophysical feedbacks trigger shifts in the modelled
  vegetation-atmosphere system at multiple scales, Biogeosciences, 7,
  1237--1245, \doi{10.5194/bg-7-1237-2010}, 2010. 

\bibitem[{Dragulji{\'c} et~al.(2012)Dragulji{\'c}, Santner, and
  Dean}]{Draguljic12aa}
Dragulji{\'c}, D., Santner, T.~J., and Dean, A.~M.: Noncollapsing Space-Filling
  Designs for Bounded Nonrectangular Regions, Technometrics, 54, 169--178,
  \doi{10.1080/00401706.2012.676951}, 2012.

\bibitem[{Fricker et~al.(2013)Fricker, Oakley, and Urban}]{Fricker2013aa}
Fricker, T.~E., Oakley, J.~E., and Urban, N.~M.: Multivariate {G}aussian
  process emulators with nonseparable covariance structures, Technometrics, 55,
  47--56, \doi{10.1080/00401706.2012.715835}, 2013.

\bibitem[{Gallassi et~al.(2009)}]{Galassi09aa}
Galassi, M., Davies, J., Theiler, J., Gough, B., Jungman, G., Booth, M., and Rossi, F.,
GNU Scientific Library Reference Manual, Network Theory Ltd.(2009), UK. \urlprefix\url{http://www. gnu. org/s/gsl}, 
2009. 

\bibitem[{Ganopolski et~al.(1998)Ganopolski, Kubatzki, Claussen, Brovkin, and
  Petoukhov}]{ganopolski98a}
Ganopolski, A., Kubatzki, C., Claussen, M., Brovkin, V., and Petoukhov, V.: The
  influence of vegetation-atmosphere-ocean interaction on climate during the
  mid-{H}olocene, Science, 280, 1916--1919,
  \doi{10.1126/science.280.5371.1916}, 1998.

\bibitem[{Goosse et~al.(2002)Goosse, Renssen, Selten, Haarsma, and
  Opsteegh}]{Goosse02aa}
Goosse, H., Renssen, H., Selten, F.~M., Haarsma, R.~J., and Opsteegh, J.~D.:
  Potential causes of abrupt climate events: A numerical study with a
  three-dimensional climate model, Geophysical Research Letters, 29, 1860,
  \doi{10.1029/2002GL014993}, 2002.

\bibitem[{Goosse et~al.(2010)Goosse, Brovkin, Fichefet, Haarsma, Huybrechts,
  Jongma, Mouchet, Selten, Barriat, Campin, Deleersnijder, Driesschaert,
  Goelzer, Janssens, Loutre, Morales~Maqueda, Opsteegh, Mathieu, Munhoven,
  Pettersson, Renssen, Roche, Schaeffer, Tartinville, Timmermann, and
  Weber}]{Goosse10aa}
Goosse, H., Brovkin, V., Fichefet, T., Haarsma, R., Huybrechts, P., Jongma, J.,
  Mouchet, A., Selten, F., Barriat, P.-Y., Campin, J.-M., Deleersnijder, E.,
  Driesschaert, E., Goelzer, H., Janssens, I., Loutre, M.-F., Morales~Maqueda,
  M.~A., Opsteegh, T., Mathieu, P.-P., Munhoven, G., Pettersson, E.~J.,
  Renssen, H., Roche, D.~M., Schaeffer, M., Tartinville, B., Timmermann, A.,
  and Weber, S.~L.: Description of the {E}arth system model of intermediate
  complexity {LOVECLIM} version 1.2, Geoscientific Model Development, 3,
  603--633, \doi{10.5194/gmd-3-603-2010}, 2010.

\bibitem[{Gramacy and Lee(2012)}]{Gramacy12aa}
Gramacy, R. and Lee, H.~H.: Cases for the nugget in modeling computer
  experiments, Statistics and Computing, 22, 713--722,
  \doi{10.1007/s11222-010-9224-x}, 2012.

\bibitem[{Henrot et~al.(2010)Henrot, Fran\c{c}ois, Favre, Butzin, Ouberdous,
  and Munhoven}]{Henrot10aa}
Henrot, A.-J., Fran\c{c}ois, L., Favre, E., Butzin, M., Ouberdous, M., and
  Munhoven, G.: Effects of CO$_{2}$, continental distribution, topography and
  vegetation changes on the climate at the Middle Miocene: a model study,
  Climate of the Past, 6, 675--694, \doi{10.5194/cp-6-675-2010}, 2010.

\bibitem[{Higdon et~al.(2008)Higdon, Gattiker, Williams, and
  Rightley}]{Higdon08aa}
Higdon, D., Gattiker, J., Williams, B., and Rightley, M.: Computer Model
  Calibration Using High-Dimensional Output, Journal of the American
  Statistical Association, 103, 570--583, \doi{10.1198/016214507000000888},
  2008.

\bibitem[{Holden et~al.(2010)Holden, Edwards, Oliver, Lenton, and
  Wilkinson}]{Holden10aa}
Holden, P., Edwards, N., Oliver, K., Lenton, T., and Wilkinson, R.: A
  probabilistic calibration of climate sensitivity and terrestrial carbon
  change in {GENIE}-1, Climate Dynamics, 35, 785--806,
  \doi{10.1007/s00382-009-0630-8}, 2010. 

\bibitem[{Homma and Saltelli(1996)}]{Homma96aa}
Homma, T. and Saltelli, A.: Importance measures in global sensitivity analysis
  of nonlinear models, Reliability Engineering \& System Safety, 52, 1--17,
  \doi{10.1016/0951-8320(96)00002-6}, 1996.

\bibitem[{Joseph and Hung(2008)}]{Joseph08aa}
Joseph, V.~R. and Hung, Y.: Orthogonal-maximin latin hypercube designs,
  Statistica Sinica, pp. 171--186, 2008.

\bibitem[{Kageyama et~al.(2004)Kageyama, Charbit, Ritz, Khodri, and
  Ramstein}]{Kageyama04aa}
Kageyama, M., Charbit, S., Ritz, C., Khodri, M., and Ramstein, G.: Quantifying
  ice-sheet feedbacks during the last glacial inception, Geophysical Research
  Letters, 31, \doi{10.1029/2004GL021339}, 2004. 

\bibitem[{Kleidon et~al.(2007)Kleidon, Fraedrich, and Low}]{Kleidon07aa}
Kleidon, A., Fraedrich, K., and Low, C.: Multiple steady-states in the
  terrestrial atmosphere-biosphere system: a result of a discrete vegetation
  classification?, Biogeosciences, 4, 707--714, \doi{10.5194/bg-4-707-2007},
  \urlprefix\url{http://www.biogeosciences.net/4/707/2007/}, 2007.

\bibitem[{Laskar et~al.(2004)Laskar, Robutel, Joutel, Boudin, Gastineau,
  Correia, and Levrard}]{Laskar04}
Laskar, J., Robutel, P., Joutel, F., Boudin, F., Gastineau, M., Correia, A.
  C.~M., and Levrard, B.: A long-term numerical solution for the insolation
  quantities of the {E}arth, Astronomy and Astrophysics, 428, 261--285,
  \doi{10.1051/0004-6361:20041335}, 2004.

\bibitem[{Lee et~al.(2011)Lee, Carslaw, Pringle, Mann, and Spracklen}]{Lee11aa}
Lee, L.~A., Carslaw, K.~S., Pringle, K.~J., Mann, G.~W., and Spracklen, D.~V.:
  Emulation of a complex global aerosol model to quantify sensitivity to
  uncertain parameters, Atmospheric Chemistry and Physics, 11,
  12\,253--12\,273, \doi{10.5194/acp-11-12253-2011}, 2011. 

\bibitem[{Lee et~al.(2013)Lee, Pringle, Reddington, Mann, Stier, Spracklen,
  Pierce, and Carslaw}]{Lee13aa}
Lee, L.~A., Pringle, K.~J., Reddington, C.~L., Mann, G.~W., Stier, P.,
  Spracklen, D.~V., Pierce, J.~R., and Carslaw, K.~S.: The magnitude and causes
  of uncertainty in global model simulations of cloud condensation nuclei,
  Atmospheric Chemistry and Physics, 13, 8879--8914,
  \doi{10.5194/acp-13-8879-2013}, 2013.

\bibitem[{Loeppky et~al.(2009)Loeppky, Sacks, and Welch}]{Loeppky09aa}
Loeppky, J.~L., Sacks, J., and Welch, W.~J.: Choosing the Sample Size of a
  Computer Experiment: A Practical Guide, Technometrics, 51, 366--376,
  \doi{10.1198/TECH.2009.08040}, \doi{10.1198/TECH.2009.08040}, 2009.

\bibitem[{Loutre(1993)}]{Loutre93aa}
Loutre, M.~F.: Param{\`e}tres orbitaux et cycles diurne et saisonnier des
  insolations, Ph.D. thesis, Universit{\'e} catholique de Louvain,
  Louvain-la-Neuve, Belgium, 1993.

\bibitem[{Loutre et~al.(2004)Loutre, Paillard, Vimeux, and Cortijo}]{Loutre04}
Loutre, M.~F., Paillard, D., Vimeux, F., and Cortijo, E.: Does mean annual
  insolation have the potential to change the climate?, Earth Planet. Sc.
  Lett., 221, 1--14, \doi{10.1016/S0012-821X(04)00108-6}, 2004.

\bibitem[{Loutre et~al.(2014)Loutre, Fichefet, Goosse, Huybrechts, Goelzer, and
  Capron}]{Loutre14aa}
Loutre, M.~F., Fichefet, T., Goosse, H., Huybrechts, P., Goelzer, H., and
  Capron, E.: Factors controlling the last interglacial climate as simulated by
  LOVECLIM1.3, Climate of the Past, 10, 1541--1565,
  \doi{10.5194/cp-10-1541-2014}, 2014. 

\bibitem[{Luethi et~al.(2008)Luethi, Le~Floch, Bereiter, Blunier, Barnola,
  Siegenthaler, Raynaud, Jouzel, Fischer, Kawamura, and Stocker}]{Luethi08aa}
Luethi, D., Le~Floch, M., Bereiter, B., Blunier, T., Barnola, J.-M.,
  Siegenthaler, U., Raynaud, D., Jouzel, J., Fischer, H., Kawamura, K., and
  Stocker, T.~F.: High-resolution carbon dioxide concentration record
  650,000-800,000 years before present, Nature, 453, 379--382,
  \doi{10.1038/nature06949}, 2008.

\bibitem[{McKay et~al.(1979)McKay, Beckman, and Conover}]{McKay79aa}
McKay, M.~D., Beckman, R.~J., and Conover, W.~J.: A Comparison of Three Methods
  for Selecting Values of Input Variables in the Analysis of Output from a
  Computer Code, Technometrics, 21, 239--245, \doi{10.2307/1268522}, 1979.

\bibitem[{Meissner et~al.(2003)Meissner, Weaver, Matthews, and
  Cox}]{Meissner03aa}
Meissner, K.~J., Weaver, A.~J., Matthews, H.~D., and Cox, P.~M.: The role of
  land surface dynamics in glacial inception: a study with the UVic Earth
  System Model, Climate Dynamics, 21, 515--537,
  \doi{10.1007/s00382-003-0352-2}, 2003. 

\bibitem[{Morris and Mitchell(1995)}]{Morris95aa}
Morris, M.~D. and Mitchell, T.~J.: Exploratory designs for computational
  experiments, Journal of Statistical Planning and Inference, 43, 381 -- 402,
  \doi{10.1016/0378-3758(94)00035-T}, 1995. 

\bibitem[{Oakley and O'Hagan(2002)}]{Oakley02aa}
Oakley, J. and O'Hagan, A.: Bayesian Inference for the Uncertainty Distribution
  of Computer Model Outputs, Biometrika, 89, 769--784,
  \doi{10.1093/biomet/89.4.769}, 2002.

\bibitem[{Oakley and O'Hagan(2004)}]{Oakley04ab}
Oakley, J.~E. and O'Hagan, A.: Probabilistic sensitivity analysis of complex
  models: a Bayesian approach, Journal of the Royal Statistical Society: Series
  B (Statistical Methodology), 66, 751--769,
  \doi{10.1111/j.1467-9868.2004.05304.x}, 2004.

\bibitem[{Opsteegh et~al.(1998)Opsteegh, Haarsma, Smelten, and
  Kattenberg}]{opsteegh98}
Opsteegh, J., Haarsma, R., Smelten, F., and Kattenberg, A.: {ECBILT}: a dynamic
  alternative to mixed boundary conditions in ocean models, Tellus, 50A,
  348--367, \doi{10.1034/j.1600-0870.1998.t01-1-00007.x}, 1998.

\bibitem[{Pepelychev(2010)}]{Pepelychev10aa}
Pepelychev, A.: The role of the nugget term in the Gaussian process method, in:
  {mODa 9} Advances in Model-Oriented Design and Analysis: In: Contributions to
  Statistics, edited by Giovagnoli, A., Atkinson, A.~C., Torsney, B., and May,
  C., pp. 149--156, Physica Verlag (Springer), Heidelberg, 2010.

\bibitem[{Petit et~al.(1999)Petit, Jouzel, Raynaud, Barkov, Barnola, Basile,
  Bender, Chappellaz, Davis, Delaygue, Delmotte, Kotlyakov, Legrand, Lipenkov,
  Lorius, Pepin, Ritz, Saltzman, and Stievenard}]{petit99}
Petit, J.~R., Jouzel, J., Raynaud, D., Barkov, N.~I., Barnola, J.-M., Basile,
  I., Bender, M., Chappellaz, J., Davis, M., Delaygue, G., Delmotte, M.,
  Kotlyakov, V.~M., Legrand, M., Lipenkov, V.~Y., Lorius, C., Pepin, L., Ritz,
  C., Saltzman, E., and Stievenard, M.: Climate and atmospheric history of the
  past 420, 000 years from the {V}ostok ice core, {A}ntarctica, Nature, 399,
  429--436, \doi{10.1038/20859}, 1999.

\bibitem[{Pollard and DeConto(2005)}]{Pollard05aa}
Pollard, D. and DeConto, R.~M.: Hysteresis in Cenozoic Antarctic ice-sheet
  variations, Global and Planetary Change, 45, 9 -- 21,
  \doi{10.1016/j.gloplacha.2004.09.011}, long-term changes in Southern
  high-latitude ice sheets and climate, the Cenozoic history, 2005.

\bibitem[{Prokopenko et~al.(2002)Prokopenko, Williams, Kuzmin, Karabanov,
  Khursevich, and Peck}]{Prokopenko02ab}
Prokopenko, A., Williams, D., Kuzmin, M., Karabanov, E., Khursevich, G., and
  Peck, J.: Muted climate variations in continental Siberia during the
  mid-Pleistocene epoch, Nature, 418, 65--68, \doi{10.1038/nature00886}, 2002.

\bibitem[{Rasmussen and Williams(2005)}]{rasmussen06aa}
Rasmussen, C. and Williams, C.: Gaussian Processes for Machine Learning,
  Adaptive Computation And Machine Learning, MIT Press, Cambridge MA, 2005.

\bibitem[{Renssen et~al.(2003)Renssen, Brovkin, Fichefet, and
  Goosse}]{Renssen03aa}
Renssen, H., Brovkin, V., Fichefet, T., and Goosse, H.: Holocene climate
  instability during the termination of the African Humid Period, Geophysical
  Research Letters, 30, n/a--n/a, \doi{10.1029/2002GL016636}, 2003.

\bibitem[{Rougier(2008)}]{Rougier08ab}
Rougier, J.: Efficient Emulators for Multivariate Deterministic Functions,
  Journal of Computational and Graphical Statistics, 17, 827--843,
  \doi{10.1198/106186008X384032}, 2008. 

\bibitem[{Rougier et~al.(2009)Rougier, Sexton, Murphy, and
  Stainforth}]{Rougier09aa}
Rougier, J., Sexton, D. M.~H., Murphy, J.~M., and Stainforth, D.: Analyzing the
  Climate Sensitivity of the {HadSM3} Climate Model Using Ensembles from
  Different but Related Experiments, Journal of Climate, 22, 3540--3557,
  \doi{10.1175/2008JCLI2533.1}, 2009.

\bibitem[{Ruddiman(2007)}]{ruddiman07rge}
Ruddiman, W.~F.: The early anthropogenic hypothesis a year later: challenges
  and responses, Reviews of Geophysics, 45, RG4001, \doi{10.1029/2006RG000207},
  2007.

\bibitem[{Sacks et~al.(1989)Sacks, Welch, Mitchell, and Wynn}]{Sacks89aa}
Sacks, J., Welch, W.~J., Mitchell, T.~J., and Wynn, H.~P.: Design and Analysis
  of Computer Experiments, Statistical Science, 4, 409--423,
  \doi{10.1214/ss/1177012413}, 1989.

\bibitem[{Saltelli et~al.(2004)Saltelli, Tarantola, Campolongo, and
  Ratto}]{Saltelli04aa}
Saltelli, A., Tarantola, S., Campolongo, F., and Ratto, M.: Sensitivity
  analysis in practite, Johx, W. Sussex, England, 2004.

\bibitem[{Sanchez-Go\~ni et~al.(1999)Sanchez-Go\~ni, Eynaud, Turon, and
  Shackleton}]{Goni99aa}
Sanchez-Go\~ni, M.~F., Eynaud, F., Turon, J.~L., and Shackleton, N.~J.: High
  resolution palynological record off the Iberian margin: direct land-sea
  correlation for the Last Interglacial complex, Earth and Planetary Science
  Letters, 171, 123--137, \doi{10.1016/S0012-821X(99)00141-7}, 1999.

\bibitem[{Santner et~al.(2003)Santner, Williams, and Notz}]{santner03}
Santner, T., Williams, B., and Notz, W.: The Design and Analysis of Computer
  Experiments, Springer, New York, 2003.

\bibitem[{Schmittner et~al.(2011)Schmittner, Urban, Shakun, Mahowald, Clark,
  Bartlein, Mix, and Rosell-Mel{\'e}}]{Schmittner11aa}
Schmittner, A., Urban, N.~M., Shakun, J.~D., Mahowald, N.~M., Clark, P.~U.,
  Bartlein, P.~J., Mix, A.~C., and Rosell-Mel{\'e}, A.: Climate Sensitivity
  Estimated from Temperature Reconstructions of the {L}ast {G}lacial {M}aximum,
  Science, 334, 1385--1388, \doi{10.1126/science.1203513},
  2011.

\bibitem[{Siegenthaler et~al.(2005)Siegenthaler, Stocker, L{\"{u}}thi,
  Schwander, Stauffer, Raynaud, Barnola, Fisher, Masson-Delmotte, and
  Jouzel}]{siegenthaler05}
Siegenthaler, U., Stocker, T.~F., L{\"{u}}thi, D., Schwander, J., Stauffer, B.,
  Raynaud, D., Barnola, J.-M., Fisher, H., Masson-Delmotte, V., and Jouzel, J.:
  Stable carbon cycle-climate relationship during the {L}ate {P}leistocene,
  Science, 310, 1313--1317,  
  \doi{10.1126/science.1120130}, 2005.

\bibitem[{Silverman(1996)}]{Silverman96aa}
Silverman, B.~W.: Smoothed functional principal components analysis by choice
  of norm, The Annals of Statistics, 24, 1--24, \doi{10.1214/aos/1033066196},
  1996.

\bibitem[{Stein and Alpert(1993)}]{stein93}
Stein, U. and Alpert, P.: Factor Separation in Numerical Simulations, Journal
  of the Atmospheric Sciences, 50, 2107--2115,
  \doi{10.1175/1520-0469(1993)050<2107:FSINS>2.0.CO;2}, 1993.

\bibitem[{Street-Perrott et~al.(1990)Street-Perrott, Mitchell, Marchand, and
  Brunner}]{Street-Perrott90aa}
Street-Perrott, F.~A., Mitchell, J. F.~B., Marchand, D.~S., and Brunner,
  J.~S.errott, F.~A.: Milankovitch and albedo forcing of the tropical monsoons:
  a comparison of geological evidence and numerical simulations for 9000 yBP,
  Earth and Environmental Science Transactions of the Royal Society of
  Edinburgh, 81, 407--427, \doi{10.1017/S0263593300020897}, 1990. 

\bibitem[{Tuenter et~al.(2003)Tuenter, Weber, Hilgen, and
  Lourens}]{Tuenter03aa}
Tuenter, E., Weber, S.~L., Hilgen, F.~J., and Lourens, L.~J.: The response of
  the African summer monsoon to remote and local forcing due to precession and
  obliquity, Global and Planetary Change, 36, 219--235,
  \doi{10.1016/S0921-8181(02)00196-0}, 2003.

\bibitem[{Urban and Fricker(2010)}]{Urban10aa}
Urban, N.~M. and Fricker, T.~E.: A comparison of Latin hypercube and grid
  ensemble designs for the multivariate emulation of an Earth system model,
  Computers \& Geosciences, 36, 746--755, \doi{10.1016/j.cageo.2009.11.004},
  2010.

\bibitem[{Vernon et~al.(2010)Vernon, Goldstein, and Bower}]{vernon2010}
Vernon, I., Goldstein, M., and Bower, R.~G.: Galaxy formation: a Bayesian
  uncertainty analysis, Bayesian Anal., 5, 619--669, \doi{10.1214/10-BA524}, 2010.

\bibitem[{Vimeux et~al.(2002)Vimeux, Cuffey, and Jouzel}]{Vimeux02aa}
Vimeux, F., Cuffey, K.~M., and Jouzel, J.: New insights into Southern
  Hemisphere temperature changes from Vostok ice cores using Deuterium excess
  correction, Earth and Planetary Science Letters, 203, 829 -- 843,
  \doi{10.1016/S0012-821X(02)00950-0}, 2002.

\bibitem[{Wilkinson(2010)}]{Wilkinson10aa}
Wilkinson, R.~D.: Bayesian Calibration of Expensive Multivariate Computer
  Experiments, in: Large-Scale Inverse Problems and Quantification of
  Uncertainty, edited by Biegler, L., Biros, G., Ghattas, O., Heinkenschloss,
  M., Keyes, D., Mallick, B., Marzouk, Y., Tenorio, L., van Bloemen~Waanders,
  B., and Willcox, K., pp. 195--215, John Wiley \& Sons, Ltd, Chichester, UK.,
  \doi{10.1002/9780470685853.ch10}, 2010.

\bibitem[{Williamson et~al.(2013)Williamson, Goldstein, Allison, Blaker,
  Challenor, Jackson, and Yamazaki}]{Williamson13ab}
Williamson, D., Goldstein, M., Allison, L., Blaker, A., Challenor, P., Jackson,
  L., and Yamazaki, K.: History matching for exploring and reducing climate
  model parameter space using observations and a large perturbed physics
  ensemble, Climate Dynamics, 41, 1703--1729, \doi{10.1007/s00382-013-1896-4}, 2013. 

\bibitem[{Williamson et~al.(2014)Williamson, Blaker, Hampton, and
  Salter}]{Williamson14aa}
Williamson, D., Blaker, A., Hampton, C., and Salter, J.: Identifying and
  removing structural biases in climate models with history matching, Climate
  Dynamics, pp. 1--26, \doi{10.1007/s00382-014-2378-z}, 2014. 

\bibitem[{Wohlfahrt et~al.(2004)Wohlfahrt, Harrison, and
  Braconnot}]{Wohlfahrt06mh}
Wohlfahrt, J., Harrison, S.~P., and Braconnot, P.: Synergistic feedbacks
  between ocean and vegetation on mid- and high-latitude climates during the
  mid-{H}olocene, Climate Dynamics, 22, 223--238,
  \doi{10.1007/s00382-003-0379-4}, 2004.

\bibitem[{Yin(2013)}]{Yin13aa}
Yin, Q.: Insolation-induced mid-Brunhes transition in Southern Ocean
  ventilation and deep-ocean temperature, Nature, 494, 222--225,
  \doi{10.1038/nature11790}, 2013.

\bibitem[{Yin and Berger(2012)}]{Yin12aa}
Yin, Q. and Berger, A.: Individual contribution of insolation and CO2 to the
  interglacial climates of the past 800,000 years, Climate Dynamics, 38,
  709--724, \doi{10.1007/s00382-011-1013-5}, 2012.

\bibitem[{Zhao et~al.(2007)Zhao, Braconnot, Harrison, Yiou, and
  Marti}]{Zhao07aa}
Zhao, Y., Braconnot, P., Harrison, S.~P., Yiou, P., and Marti, O.: Simulated
  changes in the relationship between tropical ocean temperatures and the
  western African monsoon during the mid-Holocene, Climate Dynamics, 28,
  533--551, \doi{10.1007/s00382-006-0196-7}, 2007.

\end{thebibliography}
